package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			if(text.length() == 0) {
				return " ".repeat(width);
			}
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			int extra = (width-text.length());
			return " ".repeat(extra) + text;
			
		}

		public String flushLeft(String text, int width) {
			int extra = (width-text.length());
			return text + " ".repeat(extra);
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}};
		
	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testCenter() {
		//sjekker at man fjerner et mellomrom dersom det blir ujevn fordeling
		assertEquals(" A ", aligner.center("A", 4));
		//man skal ikke legge til mellomrom for da går man over bredden.
		assertNotEquals("   A   ", aligner.center("A", 6));
		
		assertEquals("     ", aligner.center("", 5));
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		
		
	}
	@Test
	void testFlushRight() {
		assertEquals("   foo", aligner.flushRight("foo", 6));
	}
	@Test
	void testFlushLeft() {
		assertEquals("foo   ", aligner.flushLeft("foo", 6));
	}
}
